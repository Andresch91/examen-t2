﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Interfaces
{
  public  interface IPozo
    {
        List<Pozo> All();
        void Store(Pozo pozo);
        List<Pozo> ByQueryAll(string query);
        Pozo AsignarUnPozoAPersona(Pozo pozo, string nombrePersona);
      
    }
}
