﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
using System.Web.Mvc;
using AplicacionT2Calidad;
using AplicacionT2Calidad.Web.Controllers;
using Moq;
using Interfaces;
using Modelos;
using Validator.PozoValidator;



namespace AplicacionT2.Test
{
    [TestFixture]
    public class CreacionPozoTest
    {
        [Test]
        public void RetornarVista()
        {
            var controller = new CreacionPozoController(null, null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Create", view.ViewName);

        }

        [Test]
        public void TestPostCreateReturnRedirect()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<IPozo>();

            repositoryMock.Setup(o => o.Store(new Pozo()));

            var validatorMock = new Mock<PozoValidator>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            var controller = new CreacionPozoController(repositoryMock.Object, validatorMock.Object);

            var redirect = (RedirectToRouteResult)controller.Create(pozo);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreateCallStoreMethodFromRepository()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<IPozo>();

            var validatorMock = new Mock<PozoValidator>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            repositoryMock.Setup(o => o.Store(pozo));

            var controller = new CreacionPozoController(repositoryMock.Object, validatorMock.Object);

            var redirect = controller.Create(pozo);

            repositoryMock.Verify(o => o.Store(pozo), Times.Once());

        }

        [Test]

        public void TestPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var pozo = new Pozo { };

            var mock = new Mock<PozoValidator>();

            mock.Setup(o => o.Pass(pozo)).Returns(false);

            var controller = new CreacionPozoController(null, mock.Object);

            var view = controller.Create(pozo);

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }

        [Test]
        public void TestAsignarUnPozoAPersona()
        {
            var pozo = new Pozo { };

            var mock = new Mock<IPozo>();
            mock.Setup(o => o.All()).Returns(new List<Pozo>());
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());
            mock.Setup(o => o.AsignarUnPozoAPersona(pozo, "Luis")).Returns(new Pozo());
            var mockv = new Mock<PozoValidator>();

            mockv.Setup(o => o.Pass(pozo)).Returns(false);

            var controller = new CreacionPozoController(mock.Object, mockv.Object);

            var view = controller.AsignarUnPozoAPersona();

            Assert.IsInstanceOf(typeof(ViewResult), view);

        }

        //[Test]
        //public ActionResult TestRegistrarUnHistorial()
        //{

        //}
       
    }
}
