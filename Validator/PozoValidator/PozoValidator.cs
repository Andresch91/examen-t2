﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Modelos;

namespace Validator.PozoValidator
{
   public class PozoValidator
    {

       public virtual bool Pass(Pozo pozo)
       {
           if (String.IsNullOrEmpty(pozo.Nombre))
               return false;

           return true;
       }
    }
}
