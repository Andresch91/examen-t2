﻿using System.Web;
using System.Web.Mvc;

namespace AplicacionT2Calidad.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}