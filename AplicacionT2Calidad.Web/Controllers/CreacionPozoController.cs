﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Validator.PozoValidator;
using Interfaces;
using Modelos;

namespace AplicacionT2Calidad.Web.Controllers
{
    public class CreacionPozoController : Controller
    {
        //
        // GET: /CreacionPozo/

       private IPozo repository;
        private PozoValidator validator;

        public CreacionPozoController(IPozo repository, PozoValidator validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        public ViewResult Index()
        {
            return View("Inicio");
        }
        [HttpGet]
        public ViewResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(Pozo pozo)
        {

            if (validator.Pass(pozo))
            {
                repository.Store(pozo);

                TempData["UpdateSuccess"] = "Se envio el Correo Satisfactoriamente";

                return RedirectToAction("Index");
            }

            return View("Inicio", pozo);
        }

        [HttpPost]
        public ActionResult AsignarUnPozoAPersona()
        {

            return View("AsignarUnPozoAPersona");

        }

        [HttpPost]
        public ActionResult RegistrarUnHistorial()
        {

            return View("Historial");

        }
    }
}


